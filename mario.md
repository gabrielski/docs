### HTML 
https://www.tutorialspoint.com/html/index.htm

https://youtu.be/vQWlgd7hV4A



### HTTP
https://www.tutorialspoint.com/http/index.htm



### CSS
http://flexboxfroggy.com/

https://www.bitdegree.org/learn/css-tutorial

https://css-tricks.com

https://blog.logrocket.com/how-css-works-understanding-the-cascade-d181cd89a4d8/



### JavaScript
https://www.tutorialspoint.com/javascript/index.htm

https://www.zeptobook.com/javascript-fundamentals-every-beginners-should-know/

https://eloquentjavascript.net/

https://www.youtube.com/playlist?list=PL4cUxeGkcC9i9Ae2D9Ee1RvylH38dKuET



### Other Resources
https://www.tutorialspoint.com/basics_of_computer_science/index.htm

https://www.freecodecamp.org

https://dev.to/t/beginners

https://getbootstrap.com/

https://www.business2community.com/brandviews/upwork/beginners-guide-back-end-development-01855622
